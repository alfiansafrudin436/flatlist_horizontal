import React , {useState} from 'react';
import { Text, View, StyleSheet, Image, FlatList} from 'react-native';
import Constants from 'expo-constants';
import {Entypo} from '@expo/vector-icons'

export default function App() {
 const initialData=[
   {
     id:1,
     nama:'Pantai',
     image:'https://static.republika.co.id/uploads/images/inpicture_slide/pantai-ilustrasi-_171219173258-880.jpg'
   },
   {
     id:2,
     nama:'Gunung',
     image:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTyG6jlm17GeR2um5rrwoZ4bF4-SFspWhaHPw&usqp=CAU'
   },
   {
     id:3,
     nama:'Pantai',
     image:'https://www.javatravel.net/wp-content/uploads/2019/09/Wisata-Pantai-di-Semarang-1.jpg'
   },
   {
     id:4,
     nama:'Gunung',
     image:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTyG6jlm17GeR2um5rrwoZ4bF4-SFspWhaHPw&usqp=CAU'
   },
 ]
 const [dataWisata,setDataWisata]=useState(initialData);

 const renderItem=({item})=>{
   return(
     <View style={styles.item}>
      <Image source={{uri:item.image}} style={styles.image}/>
      <View style={styles.title}>
        <Entypo name='location-pin' size={30} color="white"/>
        <Text style={{color:"white"}}>{item.nama}</Text>
      </View>
     </View>
   )
 }
  return (
    <View style={styles.container}>
    <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center' }}>
      <Text style={{fontSize:20, fontWeight:'bold'}}>Location</Text>
      <Text>More place</Text>
    </View>
      <View style={{height:150}}>
        <FlatList
          horizontal
          showsHorizontalScrollIndicator={false}
          data={dataWisata}
          renderItem={renderItem}
          keyExtractor={(item)=>item.id}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
    padding: 8,
  },
  image:{
    width:300,
    height:150,
    resizeMode:'cover'
  },
  item:{
    height:150,
    marginRight:10
  },
  title:{
    flexDirection:'row',
    alignItems:'center',
    position:'absolute',
    bottom:10,
    left:10
  }
});
